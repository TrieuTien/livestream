package com.livestream.livestream.entity;

import java.util.Date;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Livestream {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String channelName;
	private String rtmpPush;
	private String streamKey;
	private String streamAlias;
	private Integer streaming;
	private boolean enableRecord;
	private boolean enableTimeShift;
	private Date timeEventStart;
	private Date timeEventfinish;
	private String hisPlayLink;
	private boolean enableTimeEvent;
	private String rtmpPull;
	
}
